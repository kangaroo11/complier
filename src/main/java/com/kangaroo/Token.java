package com.kangaroo;

/**
 * @author liubo
 * @ClassName Token
 * @Description TODO
 * @Date 2020/9/28
 * @since JDK 1.8
 */
public interface Token{

    /**
     * Token的类型
     * @return
     */
    public TokenType getType();

    /**
     * Token的文本值
     * @return
     */
    public String getText();

}