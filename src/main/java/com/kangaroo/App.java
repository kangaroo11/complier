package com.kangaroo;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        Runnable r = ()->{
            System.out.println("hello, lambda");
        };
        r.run();
    }
}
